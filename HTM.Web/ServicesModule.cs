﻿using Autofac;
using HMT.Web.Libs;
using HMT.Web.Recaptcha;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.Web
{
    public class ServicesModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(ctx =>
            {
                return new InvisibleRecaptchaValidationService(ConfigHelper.GetByKey("Recaptcha:SecretKey"));
            }).As<ICaptchaValidationService>().InstancePerRequest();

            base.Load(builder);
        }
    }
}