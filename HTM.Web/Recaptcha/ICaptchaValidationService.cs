﻿
namespace HMT.Web.Recaptcha
{
    public interface ICaptchaValidationService
    {
        bool Validate(string response);
    }
}
