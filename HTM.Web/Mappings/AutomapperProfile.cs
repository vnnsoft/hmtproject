﻿using AutoMapper;
using HMT.Model.DataModel;
using HMT.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTM.Web.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<ProductDTO, ProductViewModel>();
            CreateMap<ProductCategoriesDTO, ProductCategoriesViewModel>();
        }
    }
}