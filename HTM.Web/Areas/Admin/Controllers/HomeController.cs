﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HMT.Web.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        [Authorize(Roles = "Home_Index, Admin")]
        public ActionResult Index()
        {
            return View();
        }
    }
}