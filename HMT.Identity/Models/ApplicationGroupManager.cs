﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HMT.Identity.Models
{
    public class ApplicationGroupManager
    {
        private ApplicationGroupStore _groupStore;
        private ApplicationDbContext _db;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public ApplicationGroupManager()
        {
            _db = HttpContext.Current
                .GetOwinContext().Get<ApplicationDbContext>();
            _userManager = HttpContext.Current
                .GetOwinContext().GetUserManager<ApplicationUserManager>();
            _roleManager = HttpContext.Current
                .GetOwinContext().Get<ApplicationRoleManager>();
            _groupStore = new ApplicationGroupStore(_db);
        }


        public IQueryable<ApplicationGroup> Groups
        {
            get
            {
                return _groupStore.Groups;
            }
        }


        public async Task<IdentityResult> CreateUserGroupAsync(ApplicationGroup group)
        {
            await _groupStore.CreateAsync(group);
            return IdentityResult.Success;
        }


        public IdentityResult CreateUserGroup(ApplicationGroup group)
        {
            _groupStore.Create(group);
            return IdentityResult.Success;
        }


        public IdentityResult SetUserGroupRoles(string groupId, params string[] roleNames)
        {
            var thisGroup = this.FindById(groupId);
            thisGroup.ApplicationRoles.Clear();
            _db.SaveChanges();

            var newRole = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));
            foreach(var role in newRole)
            {
                thisGroup.ApplicationRoles.Add(new ApplicationGroupRole
                {
                    ApplicationGroupId = groupId,
                    ApplicationRoleId = role.Id,
                });
            }
            _db.SaveChanges();

            foreach(var groupUser in thisGroup.ApplicationUsers)
            {
                this.RefreshUserGroupRoles(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetUserGroupRolesAsync(string groupId, params string[] roleNames)
        {
            var thisGroup = await this.FindByIdAsync(groupId);
            thisGroup.ApplicationRoles.Clear();
            await _db.SaveChangesAsync();

            var newRoles = _roleManager.Roles.Where(r => roleNames.Any(n => n == r.Name));

            foreach(var role in newRoles)
            {
                thisGroup.ApplicationRoles.Add(new ApplicationGroupRole
                {
                    ApplicationGroupId = groupId,
                    ApplicationRoleId = role.Id
                });
            }
            await _db.SaveChangesAsync();
            foreach (var groupUser in thisGroup.ApplicationUsers)
            {

            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetUserGroupsAsync(string userId, params string[] groupIds)
        {
            var currentGroups = await this.GetUserGroupsAsync(userId);
            foreach(var group in currentGroups)
            {
                group.ApplicationUsers
                    .Remove(group.ApplicationUsers.FirstOrDefault(
                        gr => gr.ApplicationUserId == userId));
            }
            await _db.SaveChangesAsync();

            foreach (string groupId in groupIds)
            {
                var newGroup = await this.FindByIdAsync(groupId);
                newGroup.ApplicationUsers.Add(new ApplicationUserGroup
                {
                    ApplicationUserId = userId,
                    ApplicationGroupId = groupId
                });
                await _db.SaveChangesAsync();
                await this.RefreshUserGroupRolesAsync(userId);
                return IdentityResult.Success;
            }

            return IdentityResult.Success;
        }


        public IdentityResult SetUserGroups(string userId, params string[] groupIds)
        {
            var currentGroups = this.GetUserGroups(userId);
            foreach (var group in currentGroups)
            {
                group.ApplicationUsers
                    .Remove(group.ApplicationUsers.FirstOrDefault(gr => gr.ApplicationUserId == userId));
            }
            _db.SaveChanges();

            foreach(string groupId in groupIds)
            {
                var newGroup = this.FindById(groupId);
                newGroup.ApplicationUsers.Add(new ApplicationUserGroup
                {
                    ApplicationGroupId = userId,
                    ApplicationUserId = groupId
                });
            }
            _db.SaveChanges();
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> DeleteGroupAsync(string groupId)
        {

            var group = await this.FindByIdAsync(groupId);
            if(group == null)
            {
                throw new ArgumentNullException("User");
            }
            var currentGroupMambers = (await this.GetGroupUsersAsync(groupId)).ToList();

            group.ApplicationRoles.Clear();

            group.ApplicationUsers.Clear();

            _db.ApplicationGroups.Remove(group);

            await _db.SaveChangesAsync();
            foreach(var user in currentGroupMambers)
            {
                await this.RefreshUserGroupRolesAsync(user.Id);
            }
            return IdentityResult.Success;
        }


        public IdentityResult DeleteGroup(string groupId)
        {
            var group = this.FindById(groupId);
            if (group == null)
            {
                throw new ArgumentNullException("User");
            }

            var currentGroupMembers = this.GetGroupUsers(groupId).ToList();
            group.ApplicationRoles.Clear();
            group.ApplicationUsers.Clear();
            _db.ApplicationGroups.Remove(group);
            _db.SaveChanges();
            foreach(var user in currentGroupMembers)
            {
                this.RefreshUserGroupRoles(user.Id);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> UpdateGroupAsync(ApplicationGroup group)
        {
            await _groupStore.UpdateAsync(group);
            foreach(var groupUser in group.ApplicationUsers)
            {
                await this.RefreshUserGroupRolesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public IdentityResult UpdateGroup(ApplicationGroup group)
        {
            _groupStore.Update(group);
            foreach(var groupUser in group.ApplicationUsers)
            {
                this.RefreshUserGroupRoles(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public async Task<IEnumerable<ApplicationGroup>> GetUserGroupsAsync(string userId)
        {
            var result = new List<ApplicationGroup>();
            var userGroups = (from g in this.Groups
                              where g.ApplicationUsers.Any(
                                  u => u.ApplicationUserId == userId)
                              select g).ToListAsync();
            return await userGroups;
        }



        public IEnumerable<ApplicationGroup> GetUserGroups (string userId)
        {
            var reuslt = new List<ApplicationGroup>();
            var userGroups = (from g in this.Groups
                             where g.ApplicationUsers.Any(
                                 u => u.ApplicationUserId == userId)
                             select g).ToList();
            return userGroups;
        }


        public IEnumerable<ApplicationUser> GetGroupUsers(string groupId)
        {
            var group = this.FindById(groupId);
            var users = new List<ApplicationUser>();
            foreach(var groupUser in group.ApplicationUsers)
            {
                var user = _db.Users.Find(groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }


        public async Task<IEnumerable<ApplicationUser>> GetGroupUsersAsync(string groupId)
        {
            var group = await this.FindByIdAsync(groupId);
            var users = new List<ApplicationUser>();
            foreach(var groupUser in group.ApplicationUsers)
            {
                var user = await _db.Users
                    .FirstOrDefaultAsync(
                    u => u.Id == groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }


        public async Task<IEnumerable<ApplicationRole>> GetGroupRolesAsync(string groupId)
        {
            var grp = await _db.ApplicationGroups.FirstOrDefaultAsync(
                g => g.Id == groupId);
            var roles = await _roleManager.Roles.ToListAsync();
            var groupRoles = (from r in roles
                              where grp.ApplicationRoles.Any(
                                  ap => ap.ApplicationRoleId == r.Id)
                              select r).ToList();
            return groupRoles;
        }


        public IdentityResult ClearUserGroups(string userId)
        {
            return this.SetUserGroups(userId, new string[] { });
        }


        public async Task<IdentityResult> ClearUserGroupsAsync(string userId)
        {
            return await this.SetUserGroupsAsync(userId, new string[] { });
        }


        public IEnumerable<ApplicationRole> GetGroupRoles(string groupId)
        {
            var grp = _db.ApplicationGroups.FirstOrDefault(g => g.Id == groupId);
            var roles = _roleManager.Roles.ToList();
            var groupRoles = from r in roles
                             where grp.ApplicationRoles.Any(ap => ap.ApplicationRoleId == r.Id)
                             select r;
            return groupRoles;
        }


        public IdentityResult RefreshUserGroupRoles (string userId)
        {
            var user = _userManager.FindById(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }

            var oldUserRoles = _userManager.GetRoles(userId);

            if (oldUserRoles.Count > 0)
            {
                _userManager.RemoveFromRoles(userId, oldUserRoles.ToArray());
            }
            var newGroupRoles = this.GetUserGroupRoles(userId);

            var allRoles = _roleManager.Roles.ToList();

            var addTheseRoles = allRoles.Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id));

            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            _userManager.AddToRoles(userId, roleNames);

            return IdentityResult.Success;
        }


        public async Task<IdentityResult> RefreshUserGroupRolesAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }
            var oldUserRoles = await _userManager.GetRolesAsync(userId);
            if(oldUserRoles.Count > 0)
            {
                await _userManager.RemoveFromRolesAsync(userId,oldUserRoles.ToArray());
            }
            var newGroupRoles = await this.GetUserGroupRolesAsync(userId);

            var allRoles = await _roleManager.Roles.ToListAsync();
            var addTheseRoles = allRoles.Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            await _userManager.AddToRolesAsync(userId, roleNames);
            return IdentityResult.Success;
        }


        public IEnumerable<ApplicationGroupRole> GetUserGroupRoles(string userId)
        {
            var userGroups = this.GetUserGroups(userId);
            var userGroupRoles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                userGroupRoles.AddRange(group.ApplicationRoles.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<IEnumerable<ApplicationGroupRole>> GetUserGroupRolesAsync(string userId)
        {
            var userGroups = await this.GetUserGroupsAsync(userId);
            var userGroupRoles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                userGroupRoles.AddRange(group.ApplicationRoles.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<ApplicationGroup> FindByIdAsync(string id)
        {
            return await _groupStore.FindByIdAsync(id);
        }


        public ApplicationGroup FindById(string id)
        {
            return _groupStore.FindById(id);
        }
    }
}
