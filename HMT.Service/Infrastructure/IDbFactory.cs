﻿using System;
using HMT.Model;

namespace HMT.Service.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        HMTDbContext Init();
    }
}
