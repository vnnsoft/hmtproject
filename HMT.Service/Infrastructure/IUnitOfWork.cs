﻿using HMT.Service.Infrastructure;

namespace HMT.Service.Infrastructure
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
