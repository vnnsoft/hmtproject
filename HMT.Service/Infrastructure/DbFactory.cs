﻿using HMT.Model;

namespace HMT.Service.Infrastructure
{
    public class DbFactory : Disposable, IDbFactory
    {
        private HMTDbContext dbContext;

        public HMTDbContext Init()
        {
            return dbContext ?? (dbContext = new HMTDbContext());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null)
                dbContext.Dispose();
        }
    }
}
