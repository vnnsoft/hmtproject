﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMT.Model.DataModel;
using HMT.Service.Infrastructure;
using HMT.Service.Repositories;

namespace HMT.Service.Services
{
    public interface IProductCategoryService
    {
        ProductCategoriesDTO Add(ProductCategoriesDTO category);

        void Update(ProductCategoriesDTO category);

        ProductCategoriesDTO Delete(int id);

        ProductCategoriesDTO GetById(int id);
    }
    public class ProductCategoryService : IProductCategoryService
    {
        private IProductRepository _productRepository;
        private IProductCategoryRepository _productCategoryRepository;
        public ProductCategoryService
            (
             IProductCategoryRepository productCategoryRepository,
             IProductRepository productRepository
            )
        {
            this._productCategoryRepository = productCategoryRepository;
            this._productRepository = productRepository;
        }

        public ProductCategoriesDTO Add (ProductCategoriesDTO category)
        {
            return _productCategoryRepository.Add(category);
        }

        public ProductCategoriesDTO Delete(int id)
        {
            var product = _productRepository.GetMuti(x => x.CategoryId == id);
            _productRepository.DeleteMulti(x => x.CategoryId == id);

            return _productCategoryRepository.Delete(id);
        }

        public void Update(ProductCategoriesDTO category)
        {
            _productCategoryRepository.Update(category);
        }

        public ProductCategoriesDTO GetById(int id)
        {
            return _productCategoryRepository.GetSingleById(id);
        }
    }
}
