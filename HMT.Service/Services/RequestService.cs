﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMT.Model.DataModel;
using HMT.Service.Infrastructure;
using HMT.Service.Repositories;
using System.IO;

namespace HMT.Service.Services
{
    public interface IRequestService
    {
        RequestDTO Add(RequestDTO request);
        void Update(RequestDTO request);
        RequestDTO Delete(int id);
        RequestDTO GetById(int id);
    }
    public class RequestService : IRequestService
    {
        private IRequestRepository _requestRepository;
        private IProductRepository _productRepository;
        private IUnitOfWork _unitOfWork;

        public RequestService
            (
            IRequestRepository requestRepository,
            IProductRepository productRepository,
            IUnitOfWork unitOfWork
            )
        {
            this._requestRepository = requestRepository;
            this._productRepository = productRepository;
            this._unitOfWork = unitOfWork;
        }

        public RequestDTO Add(RequestDTO request)
        {
            var order = _requestRepository.Add(request);
            _unitOfWork.Commit();

            return order;
        }
        public RequestDTO Delete(int id)
        {
            throw new NotImplementedException();
        }
        public void Update(RequestDTO request)
        {
            _requestRepository.Update(request);
            _unitOfWork.Commit();
        }
        public RequestDTO GetById(int id)
        {
            return _requestRepository.GetSingleById(id);
        }
    }
}
