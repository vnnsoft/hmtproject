﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMT.Model.DataModel;
using HMT.Service.Infrastructure;
using HMT.Service.Repositories;
using System.IO;
namespace HMT.Service.Services
{
    public interface IProductService
    {
        ProductDTO Add(ProductDTO product);
        void Update(ProductDTO product);
        ProductDTO Delete(long id);
        ProductDTO GetById(long id);

        IEnumerable<ProductDTO> GetAll();
    }

    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;
        private IUnitOfWork _unitOfWork;

        public ProductService
            (
            IProductRepository productRepository,
            IUnitOfWork unitOfWork
            )
        {
            this._productRepository = productRepository;
            this._unitOfWork = unitOfWork;
        }
        public ProductDTO Add(ProductDTO Product)
        {
            var product = _productRepository.Add(Product);
            _unitOfWork.Commit();

            return product;
        }

        public void Update(ProductDTO product)
        {
            _productRepository.Update(product);
        }

        public ProductDTO Delete(long id)
        {
            return _productRepository.Delete(id);
        }
        public ProductDTO GetById(long id)
        {
            return _productRepository.GetSingleById(id);
        }

        public IEnumerable<ProductDTO> GetAll()
        {
            var products = _productRepository.GetMuti(x => x.Active == true);
            return products;
        }
    }
}
