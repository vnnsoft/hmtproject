﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMT.Model.DataModel;
using HMT.Service.Infrastructure;
namespace HMT.Service.Repositories
{
    public interface IProductRepository : IRepository<ProductDTO>
    {

        IEnumerable<ProductDTO> GetBestSellerByCat(int catId, int skip, int take);

        IEnumerable<ProductDTO> GetBestSeller(int skip, int take);

        IEnumerable<ProductDTO> GetProductByCat(int catId, int skip, int take);

        IEnumerable<ProductDTO> GetProductByCat(int catId);

        IEnumerable<ProductDTO> GetPromotionProductByCat(int catId);
    }

    public class ProductRepository : RepositoryBase<ProductDTO>, IProductRepository
    {
        public ProductRepository(IDbFactory dbFactory) 
            : base(dbFactory) { }

        public IEnumerable<ProductDTO> GetBestSellerByCat(int catId, int skip , int take)
        {
            var catIds = ChildCategoryIds(catId);

            var products = DbContext.Products.Where(p => p.Active == true && catIds.Contains(p.CategoryId))
                .OrderByDescending(p => p.ProductId)
                .Skip(skip).Take(take)
                .ToList();

            return products;
        }

        public IEnumerable<ProductDTO> GetBestSeller(int skip, int take)
        {
            var products = DbContext.Products.Where(p => p.Active == true)
                .OrderByDescending(p => p.ProductId)
                .Skip(skip).Take(take)
                .ToList();

            return products;
        }

        public List<int> ChildCategoryIds(int _parentId)
        {
            List<int> ListCats = new List<int>();

            List<int> ListSubs = new List<int>();

            ListCats.Add(_parentId);

            foreach(var cat in DbContext.ProductCategories.Where(p => p.ParentId == _parentId).ToList())
            {
                var sbus = cat.SubCategories.ToList();

                foreach(var item in sbus)
                {
                    var lastList = item.SubCategories.ToList();

                    foreach(var last in lastList)
                    {
                        ListSubs.Add(last.CategoryId);
                    }

                    ListSubs.Add(item.CategoryId);
                }

                int _catid = Convert.ToInt32(cat.CategoryId);
                ListCats.Add(_catid);
            }
            ListCats.AddRange(ListSubs);
            return ListCats;
        }

        public IEnumerable<ProductDTO> GetPromotionProductByCat(int catId)
        {
            var catIds = ChildCategoryIds(catId);

            return DbContext.Products.Where(p => catIds.Contains(p.CategoryId)).ToList();
        }

        public IEnumerable<ProductDTO> GetProductByCat (int catId, int skip, int take)
        {
            var catIds = ChildCategoryIds(catId);

                return DbContext.Products.Where(p => p.Active == true && catIds.Contains(p.CategoryId))
                                .OrderByDescending(p => p.ProductId)
                                .Skip(skip).Take(take)
                                .ToList();
        }

        public IEnumerable<ProductDTO> GetProductByCat(int catId)
        {
            var catIds = ChildCategoryIds(catId);

            return DbContext.Products.Where(p => catIds.Contains(p.CategoryId)).OrderByDescending(p => p.ProductId).ToList();
        }

    }
}
