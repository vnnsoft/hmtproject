﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMT.Service.Infrastructure;
using HMT.Model.DataModel;

namespace HMT.Service.Repositories
{
    public interface IRequestRepository : IRepository<RequestDTO>
    {
    }
    public class RequestRepository : RepositoryBase<RequestDTO>, IRequestRepository
    {
        public RequestRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
