﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMT.Service.Infrastructure;
using HMT.Model.DataModel;

namespace HMT.Service.Repositories
{
    public interface IProductCategoryRepository : IRepository<ProductCategoriesDTO>
    {
     
    }
    public class ProductCategoryRepository : RepositoryBase<ProductCategoriesDTO>, IProductCategoryRepository
    {
        public ProductCategoryRepository(IDbFactory dbFactory) : base(dbFactory)
        {
        }
    }
}
