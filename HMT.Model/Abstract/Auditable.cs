﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HMT.Model.Abstract
{
    public abstract class Auditable : IAuditable
    {
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime? CreatedDate { get; set; }
        [MaxLength(256)]
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [MaxLength(256)]
        public string UpdatedBy { get; set; }
        public bool Active { get; set; }
        [StringLength(255)]
        public string MetaTitle { get; set; }
        [StringLength(255)]
        public string MetaKeyword { get; set; }
        [StringLength(350)]
        public string MetaDescription { get; set; }
    }
}
