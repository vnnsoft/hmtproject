﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HMT.Model.DataModel;
using HMT.Model.Migrations;

namespace HMT.Model
{
    public partial class HMTDbContext : DbContext
    {
        public HMTDbContext() : base("name = HMTDbContext")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<HMTDbContext, Configuration>());
        }

        public virtual DbSet<RequestDTO> Requests { get; set; }
        public virtual DbSet<ProductDTO> Products { get; set; }
        public virtual DbSet<ProductCategoriesDTO> ProductCategories { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductCategoriesDTO>()
                 .HasMany(e => e.SubCategories)
                 .WithOptional(e => e.ParentCategory)
                 .HasForeignKey(e => e.ParentId);

        }
    }
}
