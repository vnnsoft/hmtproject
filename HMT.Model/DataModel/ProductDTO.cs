﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using HMT.Model.Abstract;

namespace HMT.Model.DataModel
{
    [Table("tblProduct")]
    public class ProductDTO : Auditable
    {
        [Key]
        public int ProductId { get; set; }

        public int CategoryId { get; set; }

        public string SKU { get; set; }

        [StringLength(255)]
        public string Url { get; set; }

        [Column(TypeName = "ntext")]
        public string ShortDesc { get; set; }

        [Column(TypeName = "ntext")]
        public string Desc { get; set; }

        public int? Amount { get; set; }

        public double? Price { get; set; }

        public string Image { get; set; }

        public string UserId { get; set; }

        public int CartAmount { get; set; }

        public double? CartPrice { get; set; }

        public double? TotalCartPrice
        {
            get
            {
                return CartPrice * CartAmount;
            }
        }

        public string SkuCart { get; set; }

        public virtual ProductCategoriesDTO ProductCategory { get; set; }
    }
}
