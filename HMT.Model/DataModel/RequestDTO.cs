﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace HMT.Model.DataModel
{
    [Table("tblRequest")]
    public partial class RequestDTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public RequestDTO()
        {
            Products = new HashSet<ProductDTO>();
        }
        public int Id { get; set; }

        public string OrderCode { get; set; }

        [Display(Name = "User")]
        public int UserId { get; set; }

        [Display(Name = "Status")]
        public int? StatusId { get; set; }

        [Display(Name = "Total amount")]
        public double? TotalAmount { get; set; }

        [Display(Name = "Total order")]
        public double? TotalOrder { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }
        public virtual ICollection<ProductDTO> Products { get; set; }
    }
}
