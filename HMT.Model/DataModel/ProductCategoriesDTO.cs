﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using HMT.Model.Abstract;

namespace HMT.Model.DataModel
{
    [Table("tblProductCategory")]
    public partial class ProductCategoriesDTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProductCategoriesDTO()
        {
            SubCategories = new HashSet<ProductCategoriesDTO>();
            Products = new HashSet<ProductDTO>();
        }
        [Key]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryFriendlyName { get; set; }
        public int? ParentId { get; set; }
        public string Desc { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductCategoriesDTO> SubCategories { get; set; }

        public virtual ProductCategoriesDTO ParentCategory { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProductDTO> Products { get; set; }
    }
}
